
import { IMangaProvider } from './IMangaProvider'

// external
import { JSDOM } from 'jsdom'

type catalogItem = {
	title : null | string,
	link  : null | string,
	cover : null | string,
	tags  : null | string[]
}

export class ChanProvider implements IMangaProvider {
	private async parseTitle(element: HTMLDivElement): Promise<null | string> {
		return element.getAttribute('title')
	}

	private async parseLink(element: HTMLDivElement): Promise<null | string> {
		const linkDom = element.querySelector('.title_link')
		return (null == linkDom) ? (null) : (linkDom.getAttribute('href'))
	}

	private async parseCover(element: HTMLDivElement): Promise<null | string> {
		const coverDiv    = element.querySelector('.manga_images') as HTMLDivElement
		const coverAnchor = coverDiv.firstElementChild as HTMLAnchorElement
		const coverImage  = coverAnchor.firstElementChild as HTMLImageElement
		return (null == coverImage) ? (null) : (coverImage.src)
	}

	private async parseTags(element: HTMLDivElement): Promise<null | string[]> {
		const tagsDiv = element.querySelector('.genre') as HTMLDivElement
		if(null == tagsDiv) { return null }
		return Array.from(tagsDiv.children).map(e => e.textContent || '').filter(e => e != '')
	}

	private async processCatalogItem(mangaCatalogItem: HTMLDivElement): Promise<catalogItem> {
		const title = this.parseTitle(mangaCatalogItem)
		const link  = this.parseLink(mangaCatalogItem)
		const cover = this.parseCover(mangaCatalogItem)
		const tags  = this.parseTags(mangaCatalogItem)

		return { title: await title, link: await link, cover: await cover, tags: await tags }
	}

	public async update() {
		const { window: windowCatalog } = await JSDOM.fromURL(this.catalog, { referrer: this.url });

		const catalog = windowCatalog.document

		for (const mangaCatalogItem of catalog.querySelectorAll('.content_row').values()) {
			const catalogItem = await this.processCatalogItem(mangaCatalogItem as HTMLDivElement)

			if(null == catalogItem.title) {
				console.log(`\nNo title`)
				continue
			}
			console.log(`\ntitle: ${catalogItem.title}`)

			if(null == catalogItem.link) {
				console.log(`${catalogItem.link} no have link`)
				continue
			}
			console.log(`link: ${windowCatalog.location.origin}${catalogItem.link}`)

			if(null == catalogItem.cover) {
				console.log(`${catalogItem.title} no have cover`)
				continue;
			}
			console.log(`cover: ${catalogItem.cover}`)

			if(null == catalogItem.tags) {
				console.log(`${catalogItem.title} no have tags`)
				continue;
			}
			console.log(`tags: ${catalogItem.tags.join(', ')}`)
		}
	}

	public constructor(url: string, catalog: string) {
		this.url     = url
		this.catalog = `${url}/${catalog}`
	}

	private url     : string
	private catalog : string
}
