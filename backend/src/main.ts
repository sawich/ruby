
import { IMangaProvider } from './mp/IMangaProvider'
import { ChanProvider }   from './mp/ChanProvider'

const launch = async () => {
	const mangaProviders: IMangaProvider[] = [
		new ChanProvider('https://mangachan.me', 'catalog'),
		new ChanProvider('http://henchan.me', 'manga')
 ]

	await Promise.all(mangaProviders.map(e => e.update()))
}

launch()


// https://mangachan.me/catalog
